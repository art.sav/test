<?php

namespace medicine\models\search;

use medicine\models\Company;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use medicine\models\Person;
use \yii\helpers\ArrayHelper;
use \yii\data\Sort;

/**
 * CompanySearch represents the model behind the search form of `common\models\Company`.
 */
class CompanySearch extends Company
{

    /** @var String  */
    protected $person;

    /** @var ActiveQuery */
    public $query;

    /** @var Integer */
    public $perPage = 20;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['slug'], 'string'],
            [['person'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->query;
        if (!$query) {
            $query = static::find();
        }


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // find persons
        if(!empty($this->person)) {

            $queryPersons = Person::find()->with(['personCompanies']);

            if(is_int($this->person)){ // в person номер модели персоны
                $queryPersons->andFilterWhere(['id' => $this->person]);
            } elseif($this->person) { // в person часть имени
                $queryPersons->andFilterWhere(['LIKE', 'name', $this->person]);
            }

            $persons = $queryPersons->asArray()->all();

            // get companies ID for every person
            $personsCompaniesId = ArrayHelper::getColumn($persons, function ($person) {

                if(!empty($person['personCompanies'])) {
                    return ArrayHelper::getColumn($person['personCompanies'], 'companyId');
                }

                return [];
            });

            // merge companies ID
            $personsCompaniesId = ArrayHelper::merge(...$personsCompaniesId);

            // merge with current company ID
            if(!empty($this->id)) {
                if(is_array($this->id)) {
                    $this->id = array_merge($this->id, $personsCompaniesId);
                } else {
                    $this->id = array_merge([$this->id], $personsCompaniesId);
                }
            } else {
                $this->id = $personsCompaniesId;
            }

            // remove duplicat companies ID and reset keys
            $this->id = array_values(array_unique($this->id));
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'slug' => $this->slug,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        $query->with('companyPersons');

        $allModels = $query->asArray()->all();

        $allModels = array_map(function($company) {
            $company['companyPersons'] = count($company['companyPersons']);
            return $company;
        }, $allModels);


        $sort = new Sort([
            'enableMultiSort' => true,
            'defaultOrder' => [
                'sortOrder' => SORT_ASC,
                'companyPersons' => SORT_DESC
            ],
            'attributes' => [
                'sortOrder',
                'companyPersons',
            ]
        ]);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
            'pagination' => [
                'pageSize' => $this->perPage,
            ],
            'sort' => $sort
        ]);

        return $dataProvider;
    }
}
